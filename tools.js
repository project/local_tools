xhrPool = [];
counter2 = 0;
(function ($, Drupal, window, document, undefined) {

    //runs function at beginning of load
    Drupal.behaviors.local_tools = {
        urlExists: function (url, linkcount, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    callback(xhr.status < 400, xhr.status);


                }
            };

            xhr.onabort = function () {
                counter2++
            };

            xhr.open('HEAD', url);
            xhr.send();
            xhrPool.push(xhr);

        },
        toolsLogin: function (settings) {
            var tools_user = Drupal.settings.local_tools.user;
            var path = '/local_tools_functions';
            var ajaxf = function (func) {
                $.ajax({
                    url: path,
                    method: 'get',
                    data: {
                        'function': func,
                        'ajax': true
                    },
                }).done(function () {
                    location.reload();
                });
            };
            if (tools_user != 0) {
                ajaxf('logout');
            } else {
                ajaxf('login');
            }
            $('#local_tools_loading').show();

        },
        attach: function (context, settings) {
            if (context === document) {
                $(document).ready(function () {
                    //function for copying
                    function LinkCopy(link) {
                        var $temp = $("<input>");
                        $("body").append($temp);
                        $temp.val(link).select();
                        document.execCommand("copy");
                        $temp.remove();
                        alert("Copied to the clipboard.");
                    }

                    //menu
                    $('#local_tools_button').on('click', function () {
                        $('#local_tools_menu').slideToggle();
                        $('#local_tools_button').toggleClass('active');
                        $('#local_tools_button .chevron').toggleClass('bottom');
                    });

                    //nid copy
                    $('.local_tools_nid_copy').on('click', function () {
                        var nid = Drupal.settings.local_tools.nid;
                        LinkCopy(nid);
                    });

                    //gid copy
                    $('.local_tools_gid_copy').on('click', function () {
                        var gid = Drupal.settings.local_tools.gid;
                        LinkCopy(gid);
                    });

                    $('.local_tools_refresh').on('click', function () {
                        var q = Math.random().toString(36).substring(7);
                        var newLoc = location.href.split('?')[0] + '?' + q;
                        $('#local_tools_loading').show();
                        location.replace(newLoc);
                    });
                    $('.local_tools_count_close').on('click', function () {
                        $('#local_tools_status').toggleClass('off');
                    });
                    $("#local_tools_count_stop").click(function () {
                        $.each(xhrPool, function (idx, xhr) {
                            xhr.abort();
                        });
                        var textx = "Stopped";
                        $('#local_tools_count_good').text(textx);
                        $('#local_tools_count_bad').text(textx);
                        $('#local_tools_count_ext').text(textx);
                        $('#local_tools_count_total').text(textx);
                        $("#local_tools_count_stop").hide();
                        $("#local_tools_count_start").show();

                        $('#local_tools_status').removeClass('active');
                        $('a').each(function () {
                            $(this).removeClass('linkext');
                            $(this).removeClass('linkbad');
                            $(this).removeClass('linkgood');
                        });
                    });
                    $("#local_tools_count_start").click(function () {
                        linkcheck();
                        $("#local_tools_count_stop").show();
                        $("#local_tools_count_start").hide();
                    });

                    function linkcheck() {
                        if (!$('#local_tools_status').hasClass('active')) {
                            $('#local_tools_status').addClass('active');
                            var badUrls = [];
                            var goodUrls = [];
                            var extUrls = [];
                            var linkCount = 1;
                            var count = $('a').length;
                            var currentCode = '';
                            $('a').each(function () {
                                if ($(this).hasClass('stopnow')) {
                                    return false;
                                }
                                var someUrl = this.href;
                                var currenta = this;

                                Drupal.behaviors.local_tools.urlExists(this.href, linkCount, function (exists, code) {
                                    var codex = code.toString();
                                    currentCode = codex;
                                    var existsx = exists.toString();
                                    if (existsx == 'true') {
                                        if (currentCode == '0') {
                                            $(currenta).addClass('linkext');
                                            extUrls.push($(currenta).text() + ': ' + someUrl + ' - Code: ' + currentCode);
                                        } else {
                                            $(currenta).addClass('linkgood');
                                            goodUrls.push($(currenta).text() + ': ' + someUrl + ' - Code: ' + currentCode);
                                        }
                                    } else {
                                        $(currenta).addClass('linkbad');
                                        badUrls.push($(currenta).text() + ': ' + someUrl + ' - Code: ' + currentCode);
                                    }
                                    var goodlength = goodUrls.length;
                                    var badlength = badUrls.length;
                                    var extlength = extUrls.length;
                                    if (linkCount >= (count)) {
                                        console.log('BAD LINKS', badUrls);
                                        console.log('GOOD LINKS', goodUrls);
                                        console.log('EXTERNAL LINKS', extUrls);
                                        console.log(counter2);
                                        //linkCount++;

                                    }
                                    //console.log(currentCode);
                                    //console.log('Checking link ' + linkCount + '/' + count + ' (' + someUrl + ')' );
                                    var countercount = linkCount + '/' + count;

                                    $('#local_tools_count_good').text(goodlength);
                                    $('#local_tools_count_bad').text(badlength);
                                    $('#local_tools_count_ext').text(extlength);
                                    $('#local_tools_count_total').text(countercount);
                                    linkCount++;

                                });
                            })
                        }
                    }

                    $('.local_tools_links').on('click', function () {
                        $('#local_tools_status').toggleClass('off');
                        linkcheck();
                    });
                    if (Drupal.settings.local_tools.nid == 'not a node') {
                        $('.local_tools_nid_copy').hide();
                    }
                    if (Drupal.settings.local_tools.gid == 'no group') {
                        $('.local_tools_gid_copy').hide();
                    }

                });
            }

        }

    };
})(jQuery, Drupal, this, this.document);
